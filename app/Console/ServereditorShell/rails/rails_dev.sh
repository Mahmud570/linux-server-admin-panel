echo "software is updating......................................
................................................................"
sudo apt-get update

echo "Installing essential software ..................................
....................................................................."
sudo apt-get -y install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev libmagickwand-dev


gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable --rails

source ~/.rvm/scripts/rvm

rvm install 2.2

rvm use 2.2

ruby -v
echo "gem: --no-document" > ~/.gemrc
gem install bundler
gem install rails

rails -v
#sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get -y install nodejs bundler
sudo apt-get -y install mysql-server mysql-client libmysqlclient-dev
gem install mysql2
cd ~
rails new testapp
cd testapp
rails server
