<?php

namespace Modules\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class DomainList extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
