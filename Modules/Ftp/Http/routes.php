<?php

Route::group(['middleware' => 'web', 'prefix' => 'ftp', 'namespace' => 'Modules\Ftp\Http\Controllers'], function()
{
    Route::any('/', [
        'as'   => 'ftp.index',
        'uses' => 'FtpController@index'
    ]);

    Route::any('create-ftp-account', [
        'as'   => 'ftp.create_account',
        'uses' => 'FtpController@shellFtpHit'
    ]);
});
