<?php

namespace Modules\Ftp\Models;

use Illuminate\Database\Eloquent\Model;

class FtpUser extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
